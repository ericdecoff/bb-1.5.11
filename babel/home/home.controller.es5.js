'use strict';

/* jshint strict: false, -W117 */
(function () {
    // Immediately Invoked Function Expression ( aka Closures )
    'use strict';

    angular.module('home', ['openWeatherServices']).controller('HomeController', HomeController).filter('roundUp', roundUp).filter('windDirection', windDirection);

    //HomeController.$inject = ['$scope','$log']    
    function HomeController($scope, $rootScope, Weather, $log, $filter) {
        $log.log('HomeController', Weather);
        $scope.cites = "2643743,2988507,4219762,3128760,2759794";
        Weather.getCities($scope.cites).then(function (data) {
            Weather.getCitiesSuccess($scope, data);
        }, function (failure) {
            $log.log('failure:', failure);
        });
        $scope.ShowForecast = function (event, index, id) {
            console.log('event', event);
            console.log('index', index);
            console.log('id', id);

            $scope.icon = angular.element(event.currentTarget.querySelector(".fa"));
            $scope.forcast = angular.element(angular.element(event.currentTarget.parentElement).parent().find('forcast')[0]);

            $scope.hidden = $scope.forcast.hasClass('hide');

            console.log('icon', $scope.icon);
            console.log('forcast', $scope.forcast);
            console.log('hidden', $scope.hidden);

            if ($scope.hidden === true) {
                console.log('hidden');
                $scope.icon.removeClass('fa-chevron-circle-up');
                $scope.icon.addClass('fa-chevron-circle-down');
                $scope.icon.addClass('weather-spin-fast');
                $scope.forcast.removeClass('hide');
                Weather.getForcast(id).then(function (data) {
                    console.log('data:', data);
                    $scope.icon.removeClass('weather-spin-fast');
                    $scope.forcastData = data;
                    //                        $scope.citiesData.list[index].forcastData = data;
                    $rootScope.$$phase || $rootScope.$apply();
                }, function (failure) {
                    console.log('failure:', failure);
                });
            } else {
                console.log('not hidden');
                $scope.icon.removeClass('fa-chevron-circle-down');
                $scope.icon.addClass('fa-chevron-circle-up');
                $scope.icon.removeClass('weather-spin-fast');
                $scope.forcast.addClass('hide');
            }
        };
    }

    function roundUp() {
        return function (value) {
            return Math.ceil(value);
        };
    }

    function windDirection() {
        return function (value) {
            var val = Math.floor(value / 22.5 + 0.5);
            var arr = ["North", "North-NE", "North-East", "East-NE", "East", "East-SE", "South-East", "South-SE", "South", "South-SW", "South-West", "West-SW", "West", "West-NW", "North-West", "North-NW"];
            return arr[val % 16];
        };
    }
})(); // Prevent global structure from being left behind ( aka logger )