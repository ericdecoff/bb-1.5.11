var gulp = require('gulp');
var sass = require('gulp-sass');
var cleanCss = require('gulp-clean-css');
var rename = require('gulp-rename');
var concat = require('gulp-concat');
var babel = require('gulp-babel');

var paths = {
    sass: ['./www/js/**/*.scss'],
    angular: [
        './node_modules/angular/angular.min.js',
        './node_modules/angular-ui-router/release/angular-ui-router.min.js'
    ]
};

gulp.task('default', ['sass']);

gulp.task('sass', function (done) {
    gulp.src(['./www/js/**/*.scss'])
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(cleanCss({
            keepSpecialComments: 0
        }))
        .pipe(concat('style-bundle.css'))        
        .pipe(rename('style.min.css' ))
        .pipe(gulp.dest('./www/css/'))
//        .pipe(gulp.dest('./www-release/css/'))
        .on('end', done);
});

gulp.task('watch', ['sass','js'], function () {
    gulp.watch(paths.sass, ['sass']);
});

gulp.task('lib', function () {
    return gulp.src(paths.angular)
        .pipe(gulp.dest('www/lib'))
//        .pipe(gulp.dest('www-release/lib'));
});

gulp.task('babel', function () {
    return gulp.src('www/js/**/*.js')
        .pipe(babel())
        .pipe(rename({ extname: '.es5.js' }))        
        .pipe(gulp.dest('babel'))
});
