# Backbase Frontend Development 2.2

## Requirements
NodeJS|Yarn|Babel
------|----|----
[(CTRL + Click )](https://github.com/EricDeCoff/NodeJS-Notes/blob/master/README.md)|[(CTRL + Click )](https://yarnpkg.com/en/docs/install)|[(CTRL + Click )](http://babeljs.io/)

## Preamble
This project is based on Node and Angular 1.5.11, I decided used Yarn package manager, since bower is no longer supported as of May 2017. Yarn also uses security centric design, using checksums to verify the integrity of every installed package before its code is executed. I did not build a `gulp release` for this assignment which would bundle and minify all the js files.

## Babel
I have included Babel using the preset es2015. 
```bash
// will create a directory called babel with the js scripts in es2015 format
// ( see js/app.js and babel/app.es5.js )

gulp babel
```

## Install from repo, fire up terminal

```bash
git clone https://bitbucket.org/ericdecoff/bb-1.5.11.git
```

## Install build environment
```bash
yarn install
```

## Gulp [ sass ] ( Build style.min.css ) 
```bash
gulp sass
```

## Gulp [ lib ] ( add library files for project ) 
```bash
gulp lib
```

## Run NodeJS Rapid Development Environment
```bash
node start
```
