(function () { // Immediately Invoked Function Expression ( aka Closures )
    'use strict';

    angular
        .module('home')
        .config(HomeConfig);

    HomeConfig.$inject = ['$stateProvider','$urlRouterProvider'];
    function HomeConfig($stateProvider, $urlRouterProvider) {
        console.log('HomeConfig:', $stateProvider, $urlRouterProvider);

        

        $stateProvider
        .state('home', {
            url: "/home",
            templateUrl: "js/home/home.html",
            controller: 'HomeController',
            controllerAS: 'vm'                    
        });

        $urlRouterProvider.otherwise("/home");        
    }

    
})(); // Prevent global structure from being left behind ( aka logger )(function() {
