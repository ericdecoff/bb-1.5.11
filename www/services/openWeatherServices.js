(function () { // Immediately Invoked Function Expression ( aka Closures )
    'use strict';

    angular
        .module('openWeatherServices',[])
        .service('Weather', OpenWeatherConfig)

    function OpenWeatherConfig($http, $q, $log) {
        $log.log('OpenWeatherConfig');
        this.$http = $http;
        this.$q = $q;
        this.units = "&units=metric"
        this.key = "&appid=3d8b309701a13f65b660fa2c64cdc517"
        this.mode = "&mode=json"

        this.getCities = function(cities) {
            $log.log('getCities:', cities);
            const defer = this.$q.defer();
            let _cities = cities != undefined ? cities : '4971068';
            let _units = this.units != undefined ? this.units : '';
            let _mode = this.mode != undefined ? this.mode : '';
            let _key = this.key != undefined ? this.key : '';

            let _openWeatherUrl = 'http://api.openweathermap.org/data/2.5/group?id=' + _cities + _units + _key + _mode 
            
            this.$http.get(_openWeatherUrl)
                .then((response) => {
                    const data = response.data;
                    defer.resolve(data);
                })
                .catch((response) => {
                    defer.reject(response);
                });
            return defer.promise;
        }
        this.getForcast = function(city) {
            $log.log('getForcast:', city);
            const defer = this.$q.defer();
            let _city = city != undefined ? city : '4971068';
            let _units = this.units != undefined ? this.units : '';
            let _mode = this.mode != undefined ? this.mode : '';
            let _key = this.key != undefined ? this.key : '';

            let _openWeatherUrl = 'http://api.openweathermap.org/data/2.5/forecast?id=' + _city + _units + _key + _mode 
            
            this.$http.get(_openWeatherUrl)
                .then((response) => {
                    const data = response.data;
                    defer.resolve(data);
                })
                .catch((response) => {
                    defer.reject(response);
                });
            return defer.promise;
        }            
     }    
})(); // Prevent global structure from being left behind ( aka logger )(function() {
